<?php

namespace GSB\CIQUALBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use GSB\CIQUALBundle\Entity\Aliment;
use GSB\CIQUALBundle\Entity\Famillealiments;

class AlimentsController extends Controller
{
    public function menuAction()//$name
    {
        $boutonsMenu = array(
            array('link' => 'gsbciqual_aliments_view', 'title' => 'Visualiser'),
            array('link' => 'gsbciqual_aliments_add', 'title' => 'Ajouter'),
            array('link' => 'gsbciqual_aliments_edit', 'title' => 'Editer'),
            array('link' => 'gsbciqual_aliments_delete', 'title' => 'Supprimer')
            /*array('link' => 'index', 'title' => 'Constituants'),
            array('link' => 'index', 'title' => 'Conditions Générales'),
            array('link' => 'index', 'title' => 'Panneau d'Administration')
             */
        );
        
        return $this->render('GSBCIQUALBundle:Aliments:menu.html.twig', array('boutonsMenu'=>$boutonsMenu));
    }
    
    public function indexAction()
    {
        // Ici, on récupérera la liste des annonces, puis on la passera au template

        // Mais pour l'instant, on ne fait qu'appeler le template
        return $this->render('GSBCIQUALBundle:Aliments:index.html.twig');
    }

    public function viewAction($id)
    {
        //On récupère l'Entity Manager
        $em = $this->getDoctrine()->getManager();
        
        if($id!=null){
            //Création de l'entité
            $aliment = new Aliment();
            $aliment = $em->getRepository('GSBCIQUALBundle:Aliment')->find($id);

            return $this->render('GSBCIQUALBundle:Aliments:view.html.twig', array(
                'aliment'=>$aliment
            ));
        }else{
            $aliments = $em->getRepository('GSBCIQUALBundle:Aliment')->findAll();
            return $this->render('GSBCIQUALBundle:Aliments:view.html.twig', array(
                'aliments' => $aliments
            ));
        }
    }

    public function addAction(Request $request)
    {


        // Si la requête est en POST, c'est que le visiteur a soumis le formulaire
        if ($request!=null && $request->isMethod('POST')) {
            //On récupère l'Entity Manager
            $em = $this->getDoctrine()->getManager();

            //Création de l'entité
            $aliment = new Aliment();
            $aliment->setNomFR('Merdouillasse');
            $aliment->setNomGB('Shittery');
            $aliment->setNomscientifique($request->request->get('nomSci'));
            $codeFam = $em->getRepository('GSBCIQUALBundle:Famillealiments')->find('19');
            $aliment->setCodeFamilleAliments($codeFam);



            //On persiste l'entité
            //$em->persist($codeFam);
            $em->persist($aliment);

            //On "flush" ce qui a été persisté
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            // Puis on redirige vers la page de visualisation de cettte annonce
            return $this->redirect($this->generateUrl('gsbciqual_aliments_view', array('id' => $aliment->getCodealiment())));
        }

        // Si on n'est pas en POST, alors on affiche le formulaire
        return $this->render('GSBCIQUALBundle:Aliments:add.html.twig');
    }

    public function editAction($id, Request $request)
    {
        //On récupère l'Entity Manager
        $em = $this->getDoctrine()->getManager();
        
        $aliment = $em->getRepository('GSBCIQUALBundle:Aliment')->find($id);

        // Même mécanisme que pour l'ajout
        if ($request!=null && $request->isMethod('POST')) {
            $nouveauAliment = new Aliment();
            $nouveauAliment->setNomfr($request->request->get('nomfr'))
                ->setNomgb($request->request->get('nomgb'))
                ->setNomscientifique($request->request->get('nomscientifique'))
                ->setCodefamillealiments($request->request->get('nomscientifique'));
            
            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien modifiée.');

            return $this->redirect($this->generateUrl('gsbciqual_aliments_view', array('id' => $request->request->get('codealiment'))));
        }

        return $this->render('GSBCIQUALBundle:Aliments:edit.html.twig', array('aliment'=>$aliment));
    }

    public function deleteAction($id)
    {
        // Ici, on récupérera l'annonce correspondant à $id

        // Ici, on gérera la suppression de l'annonce en question

        return $this->render('GSBCIQUALBundle:Aliments:delete.html.twig');
    }
}
