<?php

namespace GSB\CIQUALBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()//$name
    {
        return $this->render('GSBCIQUALBundle:Default:index.html.twig'/*, array('name' => $name)*/);
    }
    public function menuAction()//$name
    {
        $boutonsMenu = array(
            array('link' => 'gsbciqual_homepage', 'title' => 'Accueil'),
            array('link' => 'gsbciqual_aliments', 'title' => 'Aliments'),
            array('link' => 'gsbciqual_categories', 'title' => 'Catégories'),
            array('link' => 'gsbciqual_contact', 'title' => 'Contact')
            /*array('link' => 'index', 'title' => 'Constituants'),
            array('link' => 'index', 'title' => 'Conditions Générales'),
            array('link' => 'index', 'title' => 'Panneau d'Administration')
             */
        );
        
        return $this->render('GSBCIQUALBundle::menu.html.twig', array('boutonsMenu'=>$boutonsMenu));
    }
}
