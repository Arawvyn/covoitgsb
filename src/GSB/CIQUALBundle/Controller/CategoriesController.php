<?php

namespace GSB\CIQUALBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class CategoriesController extends Controller
{
    public function indexAction()
    {
        return $this->render('GSBCIQUALBundle:Default:index.html.twig'/*, array('name' => $name)*/);
    }
    public function viewAction($id, Request $request)
    {
      // On récupère notre paramètre tag
      $tag = $request->query->get('tag');

      return new Response(
        "Affichage de l'annonce d'id : ".$id.", avec le tag : ".$tag
      );
    }
}