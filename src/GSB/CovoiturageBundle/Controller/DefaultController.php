<?php

namespace GSB\CovoiturageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GSBCovoiturageBundle:Default:index.html.twig', array('name' => $name));
    }
}
