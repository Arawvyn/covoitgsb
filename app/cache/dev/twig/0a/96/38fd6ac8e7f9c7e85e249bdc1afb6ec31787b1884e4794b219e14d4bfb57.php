<?php

/* GSBCIQUALBundle:Default:contact.html.twig */
class __TwigTemplate_0a9638fd6ac8e7f9c7e85e249bdc1afb6ec31787b1884e4794b219e14d4bfb57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GSBCIQUALBundle::layout.html.twig");

        $this->blocks = array(
            'titleOnglet' => array($this, 'block_titleOnglet'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GSBCIQUALBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_titleOnglet($context, array $blocks = array())
    {
        echo "Contact";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Contact";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "  <p>
\tVous pouvez nous contacter au 06.66.66.66.66.<br>
\tOu nous envoyez un email ci-dessous.<br>
  </p>
";
    }

    public function getTemplateName()
    {
        return "GSBCIQUALBundle:Default:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  42 => 6,  36 => 5,  30 => 4,);
    }
}
