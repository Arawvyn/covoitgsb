<?php

/* GSBCIQUALBundle:Default:index.html.twig */
class __TwigTemplate_74b086516efdb0548851c90f98f6a4f9970d8de2727515fe020550959cc79280 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GSBCIQUALBundle::layout.html.twig");

        $this->blocks = array(
            'titleOnglet' => array($this, 'block_titleOnglet'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GSBCIQUALBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titleOnglet($context, array $blocks = array())
    {
        echo "Index";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Index";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "  <p>
\tBienvenue sur le site Officiel de l'application nutritionnelle WEB de GSB.<br>
\tPar le biais de l'application nutritionnelle de GSB, vous aurez accés aux composants des aliments de votre quotidien.<br>
\tEt ainsi calculer votre apport calorique journalier.<br>
\t<hr>
\tSi vous souhaitez télécharger l'application cliquez sur le bouton ci-dessous.
\t<img src=\"http://maxime.actu.com/media/img/logo.png\" width=\"175\" height=\"200\" style=\"float:right;\" />
</p>

<a href=\"logiciel.zip\"><div class=\"radius button\">Téléchargez ici </div></a>
";
    }

    public function getTemplateName()
    {
        return "GSBCIQUALBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 6,  42 => 5,  36 => 4,  30 => 3,);
    }
}
