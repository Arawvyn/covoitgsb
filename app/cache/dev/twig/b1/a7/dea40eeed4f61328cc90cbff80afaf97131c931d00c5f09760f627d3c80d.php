<?php

/* GSBCIQUALBundle:Aliments:view.html.twig */
class __TwigTemplate_b1a7dea40eeed4f61328cc90cbff80afaf97131c931d00c5f09760f627d3c80d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GSBCIQUALBundle::layout.html.twig");

        $this->blocks = array(
            'titleOnglet' => array($this, 'block_titleOnglet'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GSBCIQUALBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_titleOnglet($context, array $blocks = array())
    {
        echo "Aliments";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Aliments";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "  <p>
\t";
        // line 8
        if ((!array_key_exists("aliments", $context))) {
            // line 9
            echo "            Ton aliment à la con...<br>
            id : ";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "aliment"), "codealiment", array()), "html", null, true);
            echo "<br> nom : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "aliment"), "nomfr", array()), "html", null, true);
            echo "<br> famille : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "aliment"), "codefamillealiments", array()), "nomfr", array()), "html", null, true);
            echo "
        ";
        } else {
            // line 12
            echo "            Tes aliments dégueulasses :
            <table>
                <thead>
                    <th>Nom</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                ";
            // line 19
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "aliments"));
            foreach ($context['_seq'] as $context["_key"] => $context["alim"]) {
                // line 20
                echo "                        <tr>
                            <td>";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["alim"], "nomfr", array()), "html", null, true);
                echo "</td>
                            <td>
                                <a href=\"aliments/view/";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($context["alim"], "codealiment", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/img/view.png"), "html", null, true);
                echo "\" alt=\"VOIR\"></a>-
                                <a href=\"edit/";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute($context["alim"], "codealiment", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/img/edit.png"), "html", null, true);
                echo "\" alt=\"EDITER\"></a>-
                                <a href=\"delete/";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["alim"], "codealiment", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/img/delete.png"), "html", null, true);
                echo "\" alt=\"SUPPRIMER\"></a>
                            </td>
                        </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['alim'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "                </tbody>
            </table>
        ";
        }
        // line 32
        echo "  </p>
";
    }

    public function getTemplateName()
    {
        return "GSBCIQUALBundle:Aliments:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 32,  107 => 29,  95 => 25,  89 => 24,  83 => 23,  78 => 21,  75 => 20,  71 => 19,  62 => 12,  53 => 10,  50 => 9,  48 => 8,  45 => 7,  42 => 6,  36 => 5,  30 => 4,);
    }
}
