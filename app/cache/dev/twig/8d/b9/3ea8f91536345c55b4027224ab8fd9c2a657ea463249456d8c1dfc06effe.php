<?php

/* GSBCIQUALBundle:Aliments:menu.html.twig */
class __TwigTemplate_8db93ea8f91536345c55b4027224ab8fd9c2a657ea463249456d8c1dfc06effe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
        ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "boutonsMenu"));
        foreach ($context['_seq'] as $context["_key"] => $context["bouton"]) {
            // line 3
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath($this->getAttribute($context["bouton"], "link", array()));
            echo "\">
                <div style=\"display: inline-block;\"> ";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($context["bouton"], "title", array()), "html", null, true);
            echo " </div>
        </a>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bouton'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "GSBCIQUALBundle:Aliments:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 7,  31 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
