<?php

/* GSBCIQUALBundle:Aliments:index.html.twig */
class __TwigTemplate_07b9b831f7e9d6344436499ae9c35ef9d907db2c388b1a00799ac14072e7d9dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GSBCIQUALBundle::layout.html.twig");

        $this->blocks = array(
            'titleOnglet' => array($this, 'block_titleOnglet'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GSBCIQUALBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titleOnglet($context, array $blocks = array())
    {
        echo "Aliments";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Aliments";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"row\">";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("GSBCIQUALBundle:Aliments:menu"));
        echo "</div>
<div class=\"row\"><p>
\tVous allez pouvoir sur cette page consulter les aliments de la base de donnée CIQUAL.<br>
\tFaites votre choix à l'aide des menus.<br>
    </p>
    <a href=\"aliments/add\">Ajouter de la merdouillasse à la DB</>
    <a href=\"aliments/view\">Voir les aliments</a>
</div>
";
    }

    public function getTemplateName()
    {
        return "GSBCIQUALBundle:Aliments:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 6,  42 => 5,  36 => 4,  30 => 3,);
    }
}
