<?php

/* GSBCIQUALBundle::layout.html.twig */
class __TwigTemplate_d316c593aff8794b2567e8b4c791deb432cf325f5ab27b6d9eb287604d60a4db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'titleOnglet' => array($this, 'block_titleOnglet'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<HTML>
\t<head>
\t    <meta charset=\"utf-8\" />
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
\t\t<title>";
        // line 6
        $this->displayBlock('titleOnglet', $context, $blocks);
        echo "</title>
                <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/css/foundation.css"), "html", null, true);
        echo "\" />
\t\t<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/js/vendor/modernizr.js"), "html", null, true);
        echo "\"></script>
\t\t<script src=\"//code.jquery.com/jquery-1.11.0.min.js\"></script>
\t\t<script src=\"//code.jquery.com/jquery-migrate-1.2.1.min.js\"></script>
                <STYLE>
                        table a img {
                        width: 18px;
                        margin-left: 3px;
                        }
                </STYLE>
\t</head>
\t<body style=\"background-image: url('";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/img/bg.jpg"), "html", null, true);
        echo "'); background-size: cover;\">
\t\t<div class=\"row\">
\t\t\t<div class=\"radius callout panel\">
\t\t\t\t<div class=\"large-9 large-centered medium-9 medium-centered small-9 small-centered columns\">
\t\t\t\t\t<h1> Application Web | GSB Nutrition </h1>
                                        <BR>
                                        <h5> ";
        // line 24
        $this->displayBlock('title', $context, $blocks);
        echo " </h5>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"large-3 medium-3 small-6 columns\" style=\"opacity: 0.9;\">
\t\t\t\t";
        // line 30
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("GSBCIQUALBundle:Default:menu"));
        echo "
\t\t\t</div>
\t\t\t<div class=\"large-9 medium-9 small-6 columns\" style=\"opacity: 0.9;\">
\t\t\t\t<div class=\"radius panel\">
\t\t\t\t\t";
        // line 34
        $this->displayBlock('body', $context, $blocks);
        // line 36
        echo "\t\t\t\t</div>
\t\t\t\t<div class = \"row\">
\t\t\t\t\t<div class = \"large-7 medium-7 small-7 large-centered medium-centered small-centered large-centered columns\">
\t\t\t\t\t\t<p> All Right Reserved to GSB© copyright 2002-2015 </p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</body>
\t<script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/js/vendor/jquery.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gsb/js/foundation.min.js"), "html", null, true);
        echo "\"></script>    
    <script>\$(document).foundation();</script>
</HTML>";
    }

    // line 6
    public function block_titleOnglet($context, array $blocks = array())
    {
    }

    // line 24
    public function block_title($context, array $blocks = array())
    {
    }

    // line 34
    public function block_body($context, array $blocks = array())
    {
        // line 35
        echo "                                        ";
    }

    public function getTemplateName()
    {
        return "GSBCIQUALBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 35,  109 => 34,  104 => 24,  99 => 6,  92 => 46,  88 => 45,  77 => 36,  75 => 34,  68 => 30,  59 => 24,  50 => 18,  37 => 8,  33 => 7,  29 => 6,  22 => 1,);
    }
}
